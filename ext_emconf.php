<?php

$EM_CONF[$_EXTKEY] = array(
	'title' => 'JobControl',
	'description' => 'Job database to show job offers on your site. Paginated lists, detailview, search function, apply form and RSS. It is possible to show the most recent jobs and/or the searchform on all pages. Easy to setup yet powerful and flexible. Please ask for free support on the mailing lists, or contact the owner for paid support.',
	'category' => 'plugin',
	'constraints' => array(
		'depends' => array(
			'typo3' => '6.2.0-6.2.99'
		)
	),
	'state' => 'stable',
	'author' => 'Kevin Renskers',
	'version' => '2.15.0'
);
