<?php
defined('TYPO3_MODE') or die();

/**
 * TypoScript
 */

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
	$_EXTKEY,
	'Configuration/TypoScript/',
	'JobControl'
);

/**************************************************************/

/**
 * Table configuration array
 */

$tables = array(
	'tx_dmmjobcontrol_job' => 'Job',
	'tx_dmmjobcontrol_sector' => 'Sector',
	'tx_dmmjobcontrol_category' => 'Category',
	'tx_dmmjobcontrol_discipline' => 'Discipline',
	'tx_dmmjobcontrol_region' => 'Region',
	'tx_dmmjobcontrol_education' => 'Education',
	'tx_dmmjobcontrol_contact' => 'Contact'
);

foreach ($tables as $tableName => $tableTitle) {
	$GLOBALS['TCA'][$tableName] = array (
		'ctrl' => array (
			'title' => 'LLL:EXT:dmmjobcontrol/Resources/Private/Language/locallang_db.xlf:' . $tableName,
			'label' => 'name',
			'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/Backend/' . $tableTitle . '.png',
			'sortby' => 'sorting',
			'default_sortby' => 'ORDER BY name',
			'tstamp' => 'tstamp',
			'crdate' => 'crdate',
			'cruser_id' => 'cruser_id',
			'searchFields' =>'name',
			'languageField' => 'sys_language_uid',
			'transOrigPointerField' => 'l18n_parent',
			'transOrigDiffSourceField' => 'l18n_diffsource',
			'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/' . $tableTitle . '.php',
			'dividers2tabs' => 1
		)
	);
}

unset($tables, $tableName, $tableTitle);

/**************************************************************/

$GLOBALS['TCA']['tx_dmmjobcontrol_job']['ctrl']['label'] = 'job_title';
$GLOBALS['TCA']['tx_dmmjobcontrol_job']['ctrl']['default_sortby'] = 'ORDER BY crdate DESC';
$GLOBALS['TCA']['tx_dmmjobcontrol_job']['ctrl']['delete'] = 'deleted';
$GLOBALS['TCA']['tx_dmmjobcontrol_job']['ctrl']['enablecolumns'] = array (
	'disabled' => 'hidden',
	'starttime' => 'starttime',
	'endtime' => 'endtime'
);
$GLOBALS['TCA']['tx_dmmjobcontrol_job']['ctrl']['searchFields'] = 'job_title, employer,employer_description, location, region, short_job_description, job_description, experience, job_requirements, job_benefits';

unset(
	$GLOBALS['TCA']['tx_dmmjobcontrol_contact']['ctrl']['languageField'],
	$GLOBALS['TCA']['tx_dmmjobcontrol_contact']['ctrl']['transOrigPointerField'],
	$GLOBALS['TCA']['tx_dmmjobcontrol_contact']['ctrl']['transOrigDiffSourceField']
);

/**************************************************************/

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$_EXTKEY . '_pi1'] = 'layout, select_key';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$_EXTKEY . '_pi1'] = 'pi_flexform';

/**************************************************************/

/**
 * Plugins
 */

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
	array(
		'LLL:EXT:dmmjobcontrol/Resources/Private/Language/locallang_db.xlf:tt_content.list_type_pi1',
		$_EXTKEY . '_pi1'
	)
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
	$_EXTKEY . '_pi1',
	'FILE:EXT:dmmjobcontrol/Configuration/FlexForms/Plugin1.xml'
);
