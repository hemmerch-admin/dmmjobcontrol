<?php
defined('TYPO3_MODE') or die();

/**
 * TypoScript configuration
 */

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig('
	<INCLUDE_TYPOSCRIPT: source="DIR:EXT:' . $_EXTKEY . '/Configuration/TsConfiguration/User">
');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('
	<INCLUDE_TYPOSCRIPT: source="DIR:EXT:' . $_EXTKEY . '/Configuration/TsConfiguration/Page">
');

/**************************************************************/

/**
 * Plugins
 */

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPItoST43(
	$_EXTKEY,
	'Classes/Plugins/Plugin1.php',
	'_pi1',
	'list_type',
	0
);
