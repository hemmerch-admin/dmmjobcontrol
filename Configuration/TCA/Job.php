<?php
defined('TYPO3_MODE') or die();

/**
 * TCA configuration for the table "tx_dmmjobcontrol_job"
 */

$GLOBALS['TCA']['tx_dmmjobcontrol_job'] = array (
	'ctrl' => $GLOBALS['TCA']['tx_dmmjobcontrol_job']['ctrl'],
	'interface' => array (
		'showRecordFieldList' => 'sys_language_uid, l18n_parent, l18n_diffsource, hidden, starttime, endtime, reference, job_title, employer, employer_description, location, region, short_job_description, job_description, experience, job_requirements, job_benefits, apply_information, salary, job_type, contract_type, sector, category, discipline, education, contact'
	),
	'columns' => array (
		'sys_language_uid' => array (
			'label'  => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'exclude' => TRUE,
			'config' => array (
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array(
						'LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages',
						-1
					),
					array(
						'LLL:EXT:lang/locallang_general.xlf:LGL.default_value',
						0
					)
				)
			)
		),
		'l18n_parent' => array (
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'exclude' => TRUE,
			'config' => array (
				'type'  => 'select',
				'foreign_table' => 'tx_dmmjobcontrol_job',
				'foreign_table_where' => 'AND tx_dmmjobcontrol_job.pid=###CURRENT_PID### AND tx_dmmjobcontrol_job.sys_language_uid IN (-1,0)',
				'items' => array (
					array(
						'',
						0
					)
				)
			),
			'displayCond' => 'FIELD:sys_language_uid:>:0'
		),
		'l18n_diffsource' => array (
			'config' => array (
				'type' => 'passthrough'
			)
		),
		'hidden' => array (
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'exclude' => TRUE,
			'config' => array (
				'type' => 'check',
				'default' => 0
			)
		),
		'starttime' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'exclude' => true,
			'l10n_mode' => 'mergeIfNotBlank',
			'config' => array(
				'type' => 'input',
				'size' => 10,
				'max' => 20,
				'default' => 0,
				'eval' => 'date',
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				)
			)
		),
		'endtime' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'exclude' => true,
			'l10n_mode' => 'mergeIfNotBlank',
			'config' => array(
				'type' => 'input',
				'size' => 10,
				'max' => 20,
				'default' => 0,
				'eval' => 'date',
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				)
			)
		),
		'reference' => array (
			'label' => 'LLL:EXT:dmmjobcontrol/Resources/Private/Language/locallang_db.xlf:tx_dmmjobcontrol_job.reference',
			'exclude' => TRUE,
			'config' => array (
				'type' => 'input',
				'size' => 30
			)
		),
		'job_title' => array (
			'label' => 'LLL:EXT:dmmjobcontrol/Resources/Private/Language/locallang_db.xlf:tx_dmmjobcontrol_job.job_title',
			'config' => array (
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			)
		),
		'employer' => array (
			'label' => 'LLL:EXT:dmmjobcontrol/Resources/Private/Language/locallang_db.xlf:tx_dmmjobcontrol_job.employer',
			'exclude' => TRUE,
			'config' => array (
				'type' => 'input',
				'size' => 30
			)
		),
		'employer_description' => array (
			'label' => 'LLL:EXT:dmmjobcontrol/Resources/Private/Language/locallang_db.xlf:tx_dmmjobcontrol_job.employer_description',
			'exclude' => TRUE,
			'config' => array (
				'type' => 'text',
				'cols' => 30,
				'rows' => 5
			)
		),
		'location' => array (
			'label' => 'LLL:EXT:dmmjobcontrol/Resources/Private/Language/locallang_db.xlf:tx_dmmjobcontrol_job.location',
			'exclude' => TRUE,
			'config' => array (
				'type' => 'input',
				'size' => 30
			)
		),
		'region' => array (
			'label' => 'LLL:EXT:dmmjobcontrol/Resources/Private/Language/locallang_db.xlf:tx_dmmjobcontrol_job.region',
			'exclude' => TRUE,
			'config' => array (
				'type' => 'select',
				'foreign_table' => 'tx_dmmjobcontrol_region',
				'foreign_table_where' => 'AND tx_dmmjobcontrol_region.sys_language_uid=CAST("###REC_FIELD_sys_language_uid###" AS UNSIGNED) ORDER BY tx_dmmjobcontrol_region.uid',
				'size' => 5,
				'maxitems' => 100,
				'MM' => 'tx_dmmjobcontrol_job_region_mm',
				'wizards' => array(
					'_PADDING' => 1,
					'_VERTICAL' => 1,
					'edit' => array(
						'type' => 'popup',
						'title' => 'Edit',
						'module' => array(
							'name' => 'wizard_edit',
						),
						'popup_onlyOpenIfSelected' => TRUE,
						'icon' => 'edit2.gif',
						'JSopenParams' => 'height=350, width=580, status=0, menubar=0, scrollbars=1'
					),
					'add' => array(
						'type' => 'script',
						'title' => 'Add',
						'icon' => 'add.gif',
						'params' => array(
							'table' => 'tx_dmmjobcontrol_region',
							'pid' => '###CURRENT_PID###',
							'setValue' => 'prepend'
						),
						'module' => array(
							'name' => 'wizard_add'
						)
					),
					'list' => array(
						'type' => 'script',
						'title' => 'List',
						'icon' => 'list.gif',
						'params' => array(
							'table' => 'tx_dmmjobcontrol_region',
							'pid' => '###CURRENT_PID###'
						),
						'module' => array(
							'name' => 'wizard_list'
						)
					)
				)
			)
		),
		'short_job_description' => array (
			'label' => 'LLL:EXT:dmmjobcontrol/Resources/Private/Language/locallang_db.xlf:tx_dmmjobcontrol_job.short_job_description',
			'exclude' => TRUE,
			'config' => array (
				'type' => 'text',
				'cols' => 30,
				'rows' => 2,
			)
		),
		'job_description' => array (
			'label' => 'LLL:EXT:dmmjobcontrol/Resources/Private/Language/locallang_db.xlf:tx_dmmjobcontrol_job.job_description',
			'exclude' => TRUE,
			'config' => array (
				'type' => 'text',
				'cols' => 30,
				'rows' => 5
			)
		),
		'experience' => array (
			'label' => 'LLL:EXT:dmmjobcontrol/Resources/Private/Language/locallang_db.xlf:tx_dmmjobcontrol_job.experience',
			'exclude' => TRUE,
			'config' => array (
				'type' => 'input',
				'size' => 30
			)
		),
		'job_requirements' => array (
			'label' => 'LLL:EXT:dmmjobcontrol/Resources/Private/Language/locallang_db.xlf:tx_dmmjobcontrol_job.job_requirements',
			'exclude' => TRUE,
			'config' => array (
				'type' => 'text',
				'cols' => 30,
				'rows' => 5
			)
		),
		'job_benefits' => array (
			'label' => 'LLL:EXT:dmmjobcontrol/Resources/Private/Language/locallang_db.xlf:tx_dmmjobcontrol_job.job_benefits',
			'exclude' => TRUE,
			'config' => array (
				'type' => 'text',
				'cols' => 30,
				'rows' => 5
			)
		),
		'apply_information' => array (
			'label' => 'LLL:EXT:dmmjobcontrol/Resources/Private/Language/locallang_db.xlf:tx_dmmjobcontrol_job.apply_information',
			'exclude' => TRUE,
			'config' => array (
				'type' => 'text',
				'cols' => 30,
				'rows' => 5
			)
		),
		'salary' => array (
			'label' => 'LLL:EXT:dmmjobcontrol/Resources/Private/Language/locallang_db.xlf:tx_dmmjobcontrol_job.salary',
			'exclude' => TRUE,
			'config' => array (
				'type' => 'text',
				'cols' => 30,
				'rows' => 2,
			)
		),
		'job_type' => array (
			'label' => 'LLL:EXT:dmmjobcontrol/Resources/Private/Language/locallang_db.xlf:tx_dmmjobcontrol_job.job_type',
			'exclude' => TRUE,
			'config' => array (
				'type' => 'select',
				'items' => array (
					array(
						'LLL:EXT:dmmjobcontrol/Resources/Private/Language/locallang_db.xlf:tx_dmmjobcontrol_job.job_type.I.0',
						0
					),
					array(
						'LLL:EXT:dmmjobcontrol/Resources/Private/Language/locallang_db.xlf:tx_dmmjobcontrol_job.job_type.I.1',
						1
					)
				),
				'size' => 1
			)
		),
		'contract_type' => array (
			'label' => 'LLL:EXT:dmmjobcontrol/Resources/Private/Language/locallang_db.xlf:tx_dmmjobcontrol_job.contract_type',
			'exclude' => TRUE,
			'config' => array (
				'type' => 'select',
				'items' => array (
					array(
						'LLL:EXT:dmmjobcontrol/Resources/Private/Language/locallang_db.xlf:tx_dmmjobcontrol_job.contract_type.I.0',
						0
					),
					array(
						'LLL:EXT:dmmjobcontrol/Resources/Private/Language/locallang_db.xlf:tx_dmmjobcontrol_job.contract_type.I.1',
						1
					),
					array(
						'LLL:EXT:dmmjobcontrol/Resources/Private/Language/locallang_db.xlf:tx_dmmjobcontrol_job.contract_type.I.2',
						2
					),
					array(
						'LLL:EXT:dmmjobcontrol/Resources/Private/Language/locallang_db.xlf:tx_dmmjobcontrol_job.contract_type.I.3',
						3
					),
				),
				'size' => 1
			)
		),
		'sector' => array (
			'label' => 'LLL:EXT:dmmjobcontrol/Resources/Private/Language/locallang_db.xlf:tx_dmmjobcontrol_job.sector',
			'exclude' => TRUE,
			'config' => array (
				'type' => 'select',
				'foreign_table' => 'tx_dmmjobcontrol_sector',
				'foreign_table_where' => 'AND tx_dmmjobcontrol_sector.sys_language_uid=CAST("###REC_FIELD_sys_language_uid###" AS UNSIGNED) ORDER BY tx_dmmjobcontrol_sector.uid',
				'size' => 5,
				'maxitems' => 100,
				'MM' => 'tx_dmmjobcontrol_job_sector_mm',
				'wizards' => array(
					'_PADDING' => 1,
					'_VERTICAL' => 1,
					'edit' => array(
						'type' => 'popup',
						'title' => 'Edit',
						'module' => array(
							'name' => 'wizard_edit',
						),
						'popup_onlyOpenIfSelected' => TRUE,
						'icon' => 'edit2.gif',
						'JSopenParams' => 'height=350, width=580, status=0, menubar=0, scrollbars=1'
					),
					'add' => array(
						'type' => 'script',
						'title' => 'Add',
						'icon' => 'add.gif',
						'params' => array(
							'table' => 'tx_dmmjobcontrol_sector',
							'pid' => '###CURRENT_PID###',
							'setValue' => 'prepend'
						),
						'module' => array(
							'name' => 'wizard_add'
						)
					),
					'list' => array(
						'type' => 'script',
						'title' => 'List',
						'icon' => 'list.gif',
						'params' => array(
							'table' => 'tx_dmmjobcontrol_sector',
							'pid' => '###CURRENT_PID###'
						),
						'module' => array(
							'name' => 'wizard_list'
						)
					)
				)
			)
		),
		'category' => array (
			'label' => 'LLL:EXT:dmmjobcontrol/Resources/Private/Language/locallang_db.xlf:tx_dmmjobcontrol_job.category',
			'exclude' => TRUE,
			'config' => array (
				'type' => 'select',
				'foreign_table' => 'tx_dmmjobcontrol_category',
				'foreign_table_where' => 'AND tx_dmmjobcontrol_category.sys_language_uid=CAST("###REC_FIELD_sys_language_uid###" AS UNSIGNED) ORDER BY tx_dmmjobcontrol_category.uid',
				'size' => 5,
				'maxitems' => 100,
				'MM' => 'tx_dmmjobcontrol_job_category_mm',
				'wizards' => array(
					'_PADDING' => 1,
					'_VERTICAL' => 1,
					'edit' => array(
						'type' => 'popup',
						'title' => 'Edit',
						'module' => array(
							'name' => 'wizard_edit',
						),
						'popup_onlyOpenIfSelected' => TRUE,
						'icon' => 'edit2.gif',
						'JSopenParams' => 'height=350, width=580, status=0, menubar=0, scrollbars=1'
					),
					'add' => array(
						'type' => 'script',
						'title' => 'Add',
						'icon' => 'add.gif',
						'params' => array(
							'table' => 'tx_dmmjobcontrol_category',
							'pid' => '###CURRENT_PID###',
							'setValue' => 'prepend'
						),
						'module' => array(
							'name' => 'wizard_add'
						)
					),
					'list' => array(
						'type' => 'script',
						'title' => 'List',
						'icon' => 'list.gif',
						'params' => array(
							'table' => 'tx_dmmjobcontrol_category',
							'pid' => '###CURRENT_PID###'
						),
						'module' => array(
							'name' => 'wizard_list'
						)
					)
				)
			)
		),
		'discipline' => array (
			'label' => 'LLL:EXT:dmmjobcontrol/Resources/Private/Language/locallang_db.xlf:tx_dmmjobcontrol_job.discipline',
			'exclude' => TRUE,
			'config' => array (
				'type' => 'select',
				'foreign_table' => 'tx_dmmjobcontrol_discipline',
				'foreign_table_where' => 'AND tx_dmmjobcontrol_discipline.pid=###STORAGE_PID### AND tx_dmmjobcontrol_discipline.sys_language_uid=CAST("###REC_FIELD_sys_language_uid###" AS UNSIGNED) ORDER BY tx_dmmjobcontrol_discipline.uid',
				'size' => 5,
				'maxitems' => 100,
				'MM' => 'tx_dmmjobcontrol_job_discipline_mm',
				'wizards' => array(
					'_PADDING' => 1,
					'_VERTICAL' => 1,
					'edit' => array(
						'type' => 'popup',
						'title' => 'Edit',
						'module' => array(
							'name' => 'wizard_edit',
						),
						'popup_onlyOpenIfSelected' => TRUE,
						'icon' => 'edit2.gif',
						'JSopenParams' => 'height=350, width=580, status=0, menubar=0, scrollbars=1'
					),
					'add' => array(
						'type' => 'script',
						'title' => 'Add',
						'icon' => 'add.gif',
						'params' => array(
							'table' => 'tx_dmmjobcontrol_discipline',
							'pid' => '###CURRENT_PID###',
							'setValue' => 'prepend'
						),
						'module' => array(
							'name' => 'wizard_add'
						)
					),
					'list' => array(
						'type' => 'script',
						'title' => 'List',
						'icon' => 'list.gif',
						'params' => array(
							'table' => 'tx_dmmjobcontrol_discipline',
							'pid' => '###CURRENT_PID###'
						),
						'module' => array(
							'name' => 'wizard_list'
						)
					)
				)
			)
		),
		'education' => array (
			'label' => 'LLL:EXT:dmmjobcontrol/Resources/Private/Language/locallang_db.xlf:tx_dmmjobcontrol_job.education',
			'exclude' => TRUE,
			'config' => array (
				'type' => 'select',
				'foreign_table' => 'tx_dmmjobcontrol_education',
				'foreign_table_where' => 'AND tx_dmmjobcontrol_education.pid=###STORAGE_PID### AND tx_dmmjobcontrol_education.sys_language_uid=CAST("###REC_FIELD_sys_language_uid###" AS UNSIGNED) ORDER BY tx_dmmjobcontrol_education.uid',
				'size' => 5,
				'maxitems' => 100,
				'MM' => 'tx_dmmjobcontrol_job_education_mm',
				'wizards' => array(
					'_PADDING' => 1,
					'_VERTICAL' => 1,
					'edit' => array(
						'type' => 'popup',
						'title' => 'Edit',
						'module' => array(
							'name' => 'wizard_edit',
						),
						'popup_onlyOpenIfSelected' => TRUE,
						'icon' => 'edit2.gif',
						'JSopenParams' => 'height=350, width=580, status=0, menubar=0, scrollbars=1'
					),
					'add' => array(
						'type' => 'script',
						'title' => 'Add',
						'icon' => 'add.gif',
						'params' => array(
							'table' => 'tx_dmmjobcontrol_education',
							'pid' => '###CURRENT_PID###',
							'setValue' => 'prepend'
						),
						'module' => array(
							'name' => 'wizard_add'
						)
					),
					'list' => array(
						'type' => 'script',
						'title' => 'List',
						'icon' => 'list.gif',
						'params' => array(
							'table' => 'tx_dmmjobcontrol_education',
							'pid' => '###CURRENT_PID###'
						),
						'module' => array(
							'name' => 'wizard_list'
						)
					)
				)
			)
		),
		'contact' => array (
			'label' => 'LLL:EXT:dmmjobcontrol/Resources/Private/Language/locallang_db.xlf:tx_dmmjobcontrol_job.contact',
			'exclude' => TRUE,
			'config' => array (
				'type' => 'select',
				'items' => array (
					array(
						'',
						0
					)
				),
				'foreign_table' => 'tx_dmmjobcontrol_contact',
				'foreign_table_where' => 'ORDER BY tx_dmmjobcontrol_contact.uid',
				'size' => 1,
				'wizards' => array(
					'_PADDING' => 1,
					'_VERTICAL' => 1,
					'edit' => array(
						'type' => 'popup',
						'title' => 'Edit',
						'module' => array(
							'name' => 'wizard_edit',
						),
						'popup_onlyOpenIfSelected' => TRUE,
						'icon' => 'edit2.gif',
						'JSopenParams' => 'height=350, width=580, status=0, menubar=0, scrollbars=1'
					),
					'add' => array(
						'type' => 'script',
						'title' => 'Add',
						'icon' => 'add.gif',
						'params' => array(
							'table' => 'tx_dmmjobcontrol_contact',
							'pid' => '###CURRENT_PID###',
							'setValue' => 'prepend'
						),
						'module' => array(
							'name' => 'wizard_add'
						)
					)
				)
			)
		)
	),
	'types' => array (
		0 => array(
			'showitem' => 'sys_language_uid;;;;1-1-1, l18n_parent, l18n_diffsource, hidden;;1, reference, job_title, employer, employer_description;;;richtext:rte_transform[flag=rte_enabled|mode=ts_css], location, region, short_job_description, job_description;;;richtext:rte_transform[flag=rte_enabled|mode=ts_css], experience, job_requirements;;;richtext[paste|bold|italic|underline|formatblock|class|left|center|right|orderedlist|unorderedlist|outdent|indent|link|image]:rte_transform[mode=ts], job_benefits;;;richtext:rte_transform[flag=rte_enabled|mode=ts_css], apply_information;;;richtext:rte_transform[flag=rte_enabled|mode=ts_css], salary, job_type, contract_type, sector, category, discipline, education, contact'
		)
	),
	'palettes' => array (
		1 => array(
			'showitem' => 'starttime, endtime'
		)
	)
);
