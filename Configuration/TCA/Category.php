<?php
defined('TYPO3_MODE') or die();

/**
 * TCA configuration for the table "tx_dmmjobcontrol_category"
 */

$GLOBALS['TCA']['tx_dmmjobcontrol_category'] = array (
	'ctrl' => $GLOBALS['TCA']['tx_dmmjobcontrol_category']['ctrl'],
	'interface' => array (
		'showRecordFieldList' => 'sys_language_uid, l18n_parent, l18n_diffsource, name'
	),
	'columns' => array (
		'sys_language_uid' => array (
			'label'  => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'exclude' => TRUE,
			'config' => array (
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array(
						'LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages',
						-1
					),
					array(
						'LLL:EXT:lang/locallang_general.xlf:LGL.default_value',
						0
					)
				)
			)
		),
		'l18n_parent' => array (
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'exclude' => TRUE,
			'config' => array (
				'type'  => 'select',
				'foreign_table' => 'tx_dmmjobcontrol_category',
				'foreign_table_where' => 'AND tx_dmmjobcontrol_category.pid=###CURRENT_PID### AND tx_dmmjobcontrol_category.sys_language_uid IN (-1,0)',
				'items' => array (
					array(
						'',
						0
					)
				)
			),
			'displayCond' => 'FIELD:sys_language_uid:>:0'
		),
		'l18n_diffsource' => array (
			'config' => array (
				'type' => 'passthrough'
			)
		),
		'name' => array (
			'label' => 'LLL:EXT:dmmjobcontrol/Resources/Private/Language/locallang_db.xlf:tx_dmmjobcontrol_category.name',
			'config' => array (
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			)
		)
	),
	'types' => array (
		0 => array(
			'showitem' => 'sys_language_uid;;;;1-1-1, l18n_parent, l18n_diffsource, name'
		)
	)
);
