<?php
defined('TYPO3_MODE') or die();

/**
 * TCA configuration for the table "tx_dmmjobcontrol_contact"
 */

$GLOBALS['TCA']['tx_dmmjobcontrol_contact'] = array (
	'ctrl' => $GLOBALS['TCA']['tx_dmmjobcontrol_contact']['ctrl'],
	'interface' => array (
		'showRecordFieldList' => 'name, address, phone, email'
	),
	'columns' => array (
		'name' => array (
			'label' => 'LLL:EXT:dmmjobcontrol/Resources/Private/Language/locallang_db.xlf:tx_dmmjobcontrol_contact.name',
			'config' => array (
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			)
		),
		'address' => array (
			'label' => 'LLL:EXT:dmmjobcontrol/Resources/Private/Language/locallang_db.xlf:tx_dmmjobcontrol_contact.address',
			'exclude' => TRUE,
			'config' => array (
				'type' => 'text',
				'cols' => 30,
				'rows' => 2
			)
		),
		'phone' => array (
			'label' => 'LLL:EXT:dmmjobcontrol/Resources/Private/Language/locallang_db.xlf:tx_dmmjobcontrol_contact.phone',
			'exclude' => TRUE,
			'config' => array (
				'type' => 'input',
				'size' => 30
			)
		),
		'email' => array (
			'label' => 'LLL:EXT:dmmjobcontrol/Resources/Private/Language/locallang_db.xlf:tx_dmmjobcontrol_contact.email',
			'config' => array (
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			)
		),
	),
	'types' => array (
		1 => array(
			'showitem' => 'name, address, phone, email'
		)
	)
);
